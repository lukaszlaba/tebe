============================
Tebe - sphinx writer
============================
Tebe is a simple but powerful editor for Markdown and reStructuredText markup languages
with Sphinx and Rst2Pdf power included.
It can be used as a text editor for creating articles or even books.
You can also use it to work with any sphinx docs you have for yours project.
The mission of the project is to provide a simple and practical tool that will interest non-programer people
to use markup syntax and sphinx for any text document writing.

Changelog
---------

Tebe 0.2.8

- install process update

Tebe 0.2.2

- port to python3 and PyQt5, python 2 dropped

Tebe 0.1

- first public release (beta stage) with all main features implemented

Requirements
------------
1. Python 3
#. pyqt5
#. pyqtwebengine
#. sphinx
#. rst2pdf
#. docutils
#. recommonmark
#. mistune

How to install
--------------
Tebe is available through PyPI and can be install with pip command by typing::

   pip install tebe

To run Loge use command: ::

    tebe

On Windows system you can also find and run `tebe.exe` file in `..\Python3\Scripts` folder. For easy run make shortcut for this file

Licence
-------
Tebe is free software;
you can redistribute it and/or modify it under the terms of the **GNU General Public License**
as published by the Free Software Foundation;
either version 2 of the License,
or (at your option) any later version.

Copyright (C) 2017-2022 Lukasz Laba <lukaszlaba@gmail.com>

Contributions
-------------
If you want to help out, create a pull request or write email.

More information
----------------
Project website: https://tebe.readthedocs.io

Code repository: https://bitbucket.org/lukaszlaba/tebe

PyPI package: https://pypi.python.org/pypi/tebe

Contact: Lukasz Laba <lukaszlaba@gmail.com>
